#!/usr/bin/env python3


import cv2
import numpy
import sys

#-------------------------------------------------------------------------------
#def dotProduct(v1, v2):
#    dprod = list();
#    for i,j in zip(v1, v2):
#        dprod += i*j
#
#    return dprod
#
#-------------------------------------------------------------------------------
def computeProjection( dataV, basis ):
    projection = numpy.zeros(len(dataV))
    #print( 'len bas', len(basis), len(basis[0]), len(dataV))
    for b in basis.transpose():
        #print('len b', len(b))
        dp = dataV.dot(b)
        projection += b * dp;

    return projection

#-------------------------------------------------------------------------------
def getProjectionMatrix( M ):
  # P = M * (M'M)^(-1) * M'
  #mTransM = M.transpose().dot(M );
  #print(len(M))
  mTransMInv = numpy.linalg.inv(M.transpose() @ M);

  P = M @ mTransMInv @ M.transpose();
  return P;


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

NUM_REGIONS = 1024
FRAME_SIZE = 480*640*3  # 480x640 * 3 colors
NUM_BACKGROUND_FRAMES = 5
FRAMES_PER_SECOND = 30
START_BUFFER_DUR = 1
SAMPLE_HZ = 1

#print(cv2.__version__)
vidcap = cv2.VideoCapture(0)  # device 0
[success,image] = vidcap.read()
count = 0

backgroundFrames = list()
for i in range(NUM_BACKGROUND_FRAMES):
    [success,image] = vidcap.read()
    print ('Read a new frame: ', success)
    frameVec = numpy.reshape(image,FRAME_SIZE)
    if len(backgroundFrames):
        backgroundFrames = numpy.append(backgroundFrames, frameVec)
    else:
        backgroundFrames = frameVec

print( len(backgroundFrames))
numCols = NUM_BACKGROUND_FRAMES
backVec = backgroundFrames.reshape(int(numCols), FRAME_SIZE );
print('len backvec', len(backVec), len(backVec[0]))


[u,s,v] = numpy.linalg.svd(backVec.transpose(), full_matrices=False);

# pull some eigen vectors from u:
bg = u[0:,0:3]

print('len bg', len(bg), len(bg[0]))


# TODO:  Buffer frames
# TODO:  Learn how to write out video file -- done in another function
# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
# define the fps to be equal to 10. Also frame size is passed.
imageOut = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), FRAMES_PER_SECOND, (640,480))
residualOut = cv2.VideoWriter('resid.avi',cv2.VideoWriter_fourcc('M','J','P','G'), SAMPLE_HZ, (640,480))

imageBuffer = list()

motionDetected = False
[success,image] = vidcap.read()
framesRead = 0
while success:
    imageBuffer.append(image)
    if len(imageBuffer) > FRAMES_PER_SECOND*START_BUFFER_DUR:  imageBuffer.pop(0)

    if not framesRead % (FRAMES_PER_SECOND/SAMPLE_HZ):
        frameVec = numpy.reshape(image,FRAME_SIZE)
        residual = frameVec - computeProjection(frameVec, bg);

        stdDev = residual.std()
        print( 'resid', residual.sum(), stdDev )
        if stdDev > 20:
            motionDetected = True
            #cv2.imwrite("bgframe%d.jpg" % framesRead, output)     # save frame as JPEG file
            #cv2.imwrite("origframe%d.jpg" % framesRead, image)     # save frame as JPEG file
            foo = image;
            
            foo = numpy.reshape(numpy.uint8(residual), (480,640,3))
            residualOut.write(foo)
        else:
            motionDetected = False

    if motionDetected:
        for i in imageBuffer:
            imageOut.write(i)
        imageBuffer = list()
        

    [success,image] = vidcap.read()
    framesRead += 1


# vim: ff=unix noswf
