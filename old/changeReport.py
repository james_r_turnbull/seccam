#!/usr/bin/env python3


import cv2
import numpy
import sys
import MotionLib

NUM_BACKGROUND_FRAMES = 60
FRAMES_PER_SECOND = 30
THRESHOLD = 30  # FIXME:  Threshold should be a percent, not a fixed value
FILTER_HZ = 2

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#print(cv2.__version__)
vidcap = cv2.VideoCapture(0)  # device 0
[success,image] = vidcap.read()
count = 0



# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
# define the fps to be equal to 10. Also frame size is passed.
imageOut = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), FRAMES_PER_SECOND, (640,480))
filteredOut = cv2.VideoWriter('resid.avi',cv2.VideoWriter_fourcc('M','J','P','G'), FILTER_HZ, (640,480))

imageBuffer = list()

motionDetected = False
[success,image] = vidcap.read()
mask = numpy.array( 255 * numpy.ones((480, 640, 3)), numpy.uint8)
metThreshold = numpy.array( numpy.ones((480, 640, 3)) < 1)
framesRead = 1
while success:
    if len(imageBuffer) == NUM_BACKGROUND_FRAMES and not framesRead % int(FRAMES_PER_SECOND/FILTER_HZ):
        print('writing frame', framesRead)
        residual = abs(numpy.array(numpy.int16(image) - imageBuffer[0], numpy.int16) )
        metThreshold = (residual > THRESHOLD) | metThreshold
        filtered = image & (metThreshold * mask)
        #print(residual)
        filteredOut.write(numpy.uint8(filtered))

    imageBuffer.append(image)
    while len(imageBuffer) > NUM_BACKGROUND_FRAMES:
        imageBuffer.pop(0)

    imageOut.write(image)
    [success,image] = vidcap.read()
    framesRead += 1


# vim: ff=unix noswf
