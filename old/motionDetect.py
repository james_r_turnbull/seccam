#!/usr/bin/env python3


import cv2
import numpy
import sys



#-------------------------------------------------------------------------------

def getProjectionMatrix( M ):
  # P = M * (M'M)^(-1) * M'
  #mTransM = M.transpose().dot(M );
  #print(len(M))
  mTransM = M.transpose() @ M;
  P = M @ mTransM @ M.transpose();
  return P;


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

NUM_REGIONS = 1024
FRAME_SIZE = 480*640*3  # 480x640 * 3 colors
NUM_BACKGROUND_FRAMES = 5
regionSize = int(FRAME_SIZE/NUM_REGIONS)

#print(cv2.__version__)
#vidcap = cv2.VideoCapture('out2.mpg')
vidcap = cv2.VideoCapture('camera1')
[success,image] = vidcap.read()
count = 0

regions = [ [] for i in range(NUM_REGIONS)];
#print (regions)
for i in range(NUM_BACKGROUND_FRAMES):
  [success,image] = vidcap.read()
  print ('Read a new frame: ', success)
  frameVec = numpy.reshape(image,FRAME_SIZE)
  #a=numpy.append(a,temp);
  for r in range(NUM_REGIONS):  
    if len(regions[r]):
        #print('in') 
        regions[r] = numpy.append(regions[r], frameVec[regionSize*r:r*regionSize+regionSize])
    else:
        #print('notin') 
        regions[r] = frameVec[regionSize*r:r*regionSize+regionSize]
        #print(regions)

backgroundModel = list();
for r in regions:
    #print('Len r:  ',len(r));
    numCols = len(r) / (regionSize);
    #backVec = r.reshape(regionSize, int(numCols) );
    backVec = r.reshape( int(numCols), regionSize ).transpose();

    [u,s,v] = numpy.linalg.svd(backVec, full_matrices=False);

    # pull some eigen vectors from u:
    bg = u[0:,0:3]
    #bg = u.transpose()[0:4].transpose()
    bgP = numpy.array(getProjectionMatrix( bg) )
    backgroundModel.append(bgP);

print(len(bgP))


count = 0
while True:
  [success,image] = vidcap.read()
  print ('Read a new frame: ', success, image.std())
  frameVec = numpy.reshape(image,FRAME_SIZE)
  bg = list()
  for r in range(NUM_REGIONS):  
      temp = frameVec[regionSize*r:r*regionSize+regionSize]
      #temp = numpy.ones(regionSize) * 100;
      if len(bg):
          bg = numpy.append(bg, (temp - backgroundModel[r] @ temp))
          #bg = numpy.append(bg, backgroundModel[r] @ temp)
      else:
          bg = temp - (backgroundModel[r] @ temp)
          #bg = (backgroundModel[r] @ temp)

  standard = bg.std()
  print( bg.sum(), standard)
  if standard > 20:
      print( "found background")
      image = numpy.reshape(bg, (480,640,3));
      cv2.imwrite("bgframe%d.jpg" % count, image)     # save frame as JPEG file
      count = count+1


# vim: ff=unix
