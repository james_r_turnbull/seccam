#!/usr/bin/env python3


import cv2
import numpy
import sys
import pdb
import MotionLib

# working on this 2021-10-02


#-------------------------------------------------------------------------------
def extractPerson( TBD ):
    pass

#-------------------------------------------------------------------------------
def spectralMatchedFilter( background, newFrame ):
    pass

#-------------------------------------------------------------------------------
def getRepresentativeBackground( ):
    backgroundFrames = MotionLib.readFrames( 30 )
    return MotionLib.avgBackgroundFrame( backgroundFrames )

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# initialize background frames

bg = getRepresentativeBackground();
print("background read")

frames = MotionLib.readFrames(300);
print("sample read")

x = MotionLib.extractResolvedTarget(frames, bg)
print("processing ")

MotionLib.writeFrames( 'TestBgIn01.avi', [bg,bg,bg] );
MotionLib.writeFrames( 'TestIn01.avi', frames );
MotionLib.writeFrames( 'TestOut01.avi', x );


# vim: ff=unix noswf
