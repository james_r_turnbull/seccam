#!/usr/bin/env python3


import cv2
import numpy
import sys
import pdb
import MotionLib

# working on this 2021-10-02


#-------------------------------------------------------------------------------
def readFile(fn):
    inFile = cv2.VideoCapture(fn)
    frames = []
    [success, image] = inFile.read()
    while success:
        frames += [image]
        [success, image] = inFile.read()
    frames += [image]

    return numpy.array(frames)

    

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# initialize background frames
#bgFile = cv2.VideoCapture('TestBgIn01.avi')
#[success, bg] = bgFile.read()
bg = readFile('TestBgIn01.avi')

print("background read")

#framesFile = cv2.VideoCapture('TestIn01.avi')
#[success, frames] = framesFile.read()
frames = readFile('TestIn01.avi')
print("sample read")


for i in [0.99, 0.995, 0.998, 0.999, 0.9999]:
    x = MotionLib.extractResolvedTarget(frames, bg[0], threshold=i)
    print("processing ", i)
    outfile = "TestOut" + str(i) + ".avi"
    MotionLib.writeFrames( outfile, x );

print("done")



# vim: ff=unix noswf
