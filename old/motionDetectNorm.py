#!/usr/bin/env python3


import cv2
import numpy
import sys
import MotionLib

NUM_REGIONS = 1024
FRAME_SIZE = 480*640*3  # 480x640 * 3 colors
NUM_BACKGROUND_FRAMES = 5
FRAMES_PER_SECOND = 30
START_BUFFER_DUR = 1
SAMPLE_HZ = 1

#-------------------------------------------------------------------------------
def computeProjection( dataV, basis ):
    projection = numpy.zeros(len(dataV))
    for b in basis.transpose():
        dp = dataV.dot(b)
        projection += b * dp;

    return projection

#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#print(cv2.__version__)
vidcap = cv2.VideoCapture(0)  # device 0
[success,image] = vidcap.read()
count = 0

averageBackground = numpy.zeros(FRAME_SIZE)
for i in range(NUM_BACKGROUND_FRAMES):
    [success,image] = vidcap.read()
    print ('Read a new frame: ', success)
    frameVec = numpy.reshape(image,FRAME_SIZE)
    averageBackground += frameVec 

averageBackground = MotionLib.normal(averageBackground)

print( len(averageBackground), averageBackground.sum(), averageBackground.dot(averageBackground))


# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
# define the fps to be equal to 10. Also frame size is passed.
imageOut = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), FRAMES_PER_SECOND, (640,480))
residualOut = cv2.VideoWriter('resid.avi',cv2.VideoWriter_fourcc('M','J','P','G'), SAMPLE_HZ, (640,480))

imageBuffer = list()

motionDetected = False
[success,image] = vidcap.read()
framesRead = 0
while success:
    imageBuffer.append(image)
    if len(imageBuffer) > FRAMES_PER_SECOND*START_BUFFER_DUR:  imageBuffer.pop(0)

    if not framesRead % (FRAMES_PER_SECOND/SAMPLE_HZ):
        frameVec = numpy.array(numpy.reshape(image,FRAME_SIZE), float)
        frameMagnitude = MotionLib.magnitude(frameVec)
        frameVec = MotionLib.normal(frameVec)

        #residual = abs(frameVec - averageBackground)
        #residualSum = residual.sum()

        residual = frameVec.dot(averageBackground)
        residualV = frameVec * residual
        #residualSum = residual.sum()

        averageBackground += frameVec;
        averageBackground /= 2

        print( 'resid', residual)
        if residual < 0.99:
            motionDetected = True
            avgBgIm = numpy.reshape(numpy.uint8(frameMagnitude*averageBackground),(480,640,3))
            #cv2.imwrite("bgframe%d.jpg" % framesRead, avgBgIm) # save frame as JPEG file
            #cv2.imwrite("origframe%d.jpg" % framesRead, image)     # save frame as JPEG file
            foo = numpy.reshape(numpy.uint8(residualV), (480,640,3))
            residualOut.write(avgBgIm)
        else:
            motionDetected = False

    if motionDetected:
        for i in imageBuffer:
            imageOut.write(i)
        imageBuffer = list()
        

    [success,image] = vidcap.read()
    framesRead += 1


# vim: ff=unix noswf
