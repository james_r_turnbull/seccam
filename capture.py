#!/usr/bin/env python3

import pdb

import cv2
import numpy
import sys
import pdb
import MotionLib
import threading
import time

#frameBuffer = numpy.array([])
inFrameBuffer = []
extractFrameBuffer = []
backgroundBuffer = []
pixelThresholds = []
READY_TO_ROCK = False

# working on this 2021-10-10
# Live system framework

#-------------------------------------------------------------------------------
# read from camera, write to buffer
def readForever(mutex):
    global inFrameBuffer
    vidcap = cv2.VideoCapture(0)  # device 0
    [success, image] = vidcap.read()
    inFrameBuffer = [image]
    while success:
        [success, image] = vidcap.read()
        mutex.acquire()
        inFrameBuffer += [image]
        mutex.release()
    return numpy.array(frames)

#-------------------------------------------------------------------------------
def thresholdImage(background, image):
    mask = numpy.array(MotionLib.findResolvedTarget( image, background, threshold=0.9999 ), dtype="uint8")
    #filteredMask = cv2.medianBlur(mask, ksize=5) > 0


    #mask = numpy.array(image - background) > 10

    filteredMask = mask
    #threshold = numpy.array(1*(abs(1.0*frame - background) > 30), dtype="uint8")
    #filteredMask = cv2.medianBlur(threshold, ksize=5) > 0
    metThreshold = False
    if filteredMask.sum() > 10:
        metThreshold = True
    print('Number of thresholded pixels:  ', filteredMask.sum())
    return metThreshold


    # adjust pixelThresholds.. recall lower is less sensitive
    # thresholded pixels value gets smaller, non-threshold ones remain same
    #pixelThresholds = (mask * pixelThresholds * defaultPixelThreshold) + (mask < 1) * pixelThresholds
    #hotPixels = hotPixels - numpy.ones(hotPixels.shape)
    # raise all thresholds
    #pixelThresholds = pixelThresholds / defaultPixelThreshold
    # but not beyond default
    #pixelThresholds = (pixelThresholds > defaultPixelThreshold)*
    
#-------------------------------------------------------------------------------
def thresholdFrames(mutexRead, mutexExtract, mutexBackground, mutexThreshold):
    # get background
    global inFrameBuffer
    global extractFrameBuffer
    global backgroundBuffer
    global pixelThresholds


    while not READY_TO_ROCK:
        time.sleep(0.5)

    while True:
        length = len(inFrameBuffer)
        if length > 30:
            mutexRead.acquire()
            # process last 30
            temp = numpy.array(inFrameBuffer[-30:]);
            # but delete first 30 
            del inFrameBuffer[0:30]
            mutexRead.release()

            #frame = MotionLib.averageFrames([temp[0], temp[15], temp[28]])
            #frame = cv2.medianBlur(temp[15], ksize=5)
            #frame = temp[15]
            
            #frame = numpy.mean([temp[1],temp[14],temp[29]], axis=0)
            #frame = numpy.array(frame, dtype="uint8")


            mutexBackground.acquire()
            backN = numpy.array(backgroundBuffer)
            mutexBackground.release()

            foundTarget = thresholdImage(backN, temp[1])
            foundTarget |= thresholdImage(backN, temp[14])
            foundTarget |= thresholdImage(backN, temp[29])

            if foundTarget:
                mutexExtract.acquire()
                if len(extractFrameBuffer) == 0:
                    extractFrameBuffer = numpy.array(temp, dtype='uint8')
                else:
                    extractFrameBuffer += temp
                print('extracted frames', len(extractFrameBuffer))
                mutexExtract.release()
            else:
                print('discarded frames')

        time.sleep(0.1)

#-------------------------------------------------------------------------------
def tuneBackground(mutexRead, mutexBackground, mutexThreshold):
    global pixelThresholds
    global backgroundBuffer
    global READY_TO_ROCK


    while True:
        while len(inFrameBuffer) < 10:
            time.sleep(0.5)
        
        mutexRead.acquire()
        temp = numpy.array(inFrameBuffer[0:9])
        # Discard background frames:
        #del inFrameBuffer[0:5]
        testImage = numpy.array(inFrameBuffer[-1])
        mutexRead.release()

        #background = numpy.array(MotionLib.averageFrames(temp), dtype="uint8");
        medFrame = numpy.median(temp, axis=0);
        mutexBackground.acquire()
        #temp2 = [cv2.medianBlur(i, ksize=5) for i in temp]
#        backN = MotionLib.colorNormalize(background)
#        tempBack = numpy.array(backN)
#        backgroundBuffer = backN
        #backgroundBuffer = cv2.medianBlur(background, ksize=5)
        backgroundBuffer = numpy.array(medFrame);
        mutexBackground.release()
        print('Background established')

        # establish initial threhsold
#        mutexThreshold.acquire()
#        pixelThresholds = MotionLib.getDotProduct( testImage, tempBack ) * 0.99
#        mutexThreshold.release()
#        print('Threshold established')

        READY_TO_ROCK = True
        time.sleep(15)  # reset background every 5 seconds




#-------------------------------------------------------------------------------
def extract(mutexExtract, mutextRead, mutexBackground):

    fps = 30
    filename = 'extract.avi'
    framesOut = cv2.VideoWriter( filename, cv2.VideoWriter_fourcc('M','J','P','G'), fps, (640,480) )
    rawOut = cv2.VideoWriter( 'raw.avi', cv2.VideoWriter_fourcc('M','J','P','G'), fps, (640,480) )
    bgOut = cv2.VideoWriter( 'background.avi', cv2.VideoWriter_fourcc('M','J','P','G'), fps, (640,480) )
    global extractFrameBuffer
    while True:
        if len(extractFrameBuffer) > 0:
            mutexRead.acquire()
            rawFrames = numpy.array(inFrameBuffer)
            mutextRead.release()
            for f in rawFrames:
                rawOut.write(numpy.array(f, dtype="uint8"))


            mutexExtract.acquire()
            extractionFrames = numpy.array(extractFrameBuffer)
            extractFrameBuffer = []
            mutexExtract.release()

            global background
            mutexBackground.acquire()
            background = numpy.array(backgroundBuffer)
            mutexBackground.release()

            #extraction = extractionFrames
            extraction = MotionLib.extractResolvedTarget(extractionFrames, background, threshold=0.99)
            print('extracted, writing', extraction.shape)
            bgOut.write(numpy.array(background, dtype="uint8"))
            for f in extraction:
                framesOut.write(numpy.array(f, dtype="uint8"))
    



#-------------------------------------------------------------------------------
mutexRead = threading.Lock()
mutexBackground = threading.Lock()
mutexExtract = threading.Lock()
mutexThreshold = threading.Lock()

 
thread_read = threading.Thread(target = readForever, args = (mutexRead,))
thread_threshold = threading.Thread(target = thresholdFrames, args = (mutexRead,mutexExtract,mutexBackground,mutexThreshold,))
thread_extract = threading.Thread(target = extract, args = (mutexExtract,mutexRead,mutexBackground,))
thread_tuneBackground = threading.Thread(target = tuneBackground, args = (mutexRead,mutexBackground,mutexThreshold,))

thread_read.start()
thread_tuneBackground.start()
thread_threshold.start()
thread_extract.start()


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------





# vim: ff=unix noswf
