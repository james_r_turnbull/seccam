#!/usr/bin/env python3


import cv2
import numpy
import sys
import MotionLib

NUM_REGIONS = 1024
FRAME_SIZE = 480*640*3  # 480x640 * 3 colors
NUM_BACKGROUND_FRAMES = 5
FRAMES_PER_SECOND = 30
START_BUFFER_DUR = 1
FILTER_HZ = 30

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#print(cv2.__version__)
vidcap = cv2.VideoCapture(0)  # device 0
[success,image] = vidcap.read()
count = 0



# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
# define the fps to be equal to 10. Also frame size is passed.
imageOut = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), FRAMES_PER_SECOND, (640,480))
filteredOut = cv2.VideoWriter('resid.avi',cv2.VideoWriter_fourcc('M','J','P','G'), FILTER_HZ, (640,480))

imageBuffer = list()

motionDetected = False
[success,image] = vidcap.read()
framesRead = 0
while success:
    imageBuffer.append(image)
    #while len(imageBuffer) > 180:
        #imageBuffer.pop(0)


    filteredOut.write(sum(imageBuffer))
    imageOut.write(image)

    [success,image] = vidcap.read()
    framesRead += 1


# vim: ff=unix noswf
