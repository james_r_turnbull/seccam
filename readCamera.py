#!/usr/bin/env python3

import pdb

import cv2
import numpy
import sys
import pdb
import MotionLib
import threading
import time

#frameBuffer = numpy.array([])
inFrameBuffer = []
extractFrameBuffer = []
backgroundBuffer = []
pixelThresholds = []
READY_TO_ROCK = False

# working on this 2021-10-10

#-------------------------------------------------------------------------------
# read from camera, write to buffer
def readForever(mutex):
    global inFrameBuffer
    vidcap = cv2.VideoCapture(0)  # device 0
    [success, image] = vidcap.read()
    inFrameBuffer = [image]
    while success:
        [success, image] = vidcap.read()
        mutex.acquire()
        inFrameBuffer += [image]
        mutex.release()
    return numpy.array(frames)

#-------------------------------------------------------------------------------
def extract( mutextRead ):

    fps = 30
    rawOut = cv2.VideoWriter( 'rawCamera.avi', cv2.VideoWriter_fourcc('M','J','P','G'), fps, (640,480) )
    global inFrameBuffer
    while True:
        if len(inFrameBuffer) >= 30:
            mutexRead.acquire()
            rawFrames = numpy.array(inFrameBuffer)
            del inFrameBuffer[0:len(rawFrames)]
            mutextRead.release()
            for f in rawFrames:
                rawOut.write(numpy.array(f, dtype="uint8"))

            print('extracted, writing', rawFrames.shape)

#-------------------------------------------------------------------------------
mutexRead = threading.Lock()
mutexExtract = threading.Lock()

 
thread_read = threading.Thread(target = readForever, args = (mutexRead,))
thread_extract = threading.Thread(target = extract, args = (mutexRead,))

thread_read.start()
thread_extract.start()


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------





# vim: ff=unix noswf
