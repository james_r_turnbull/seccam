#!/usr/bin/python3


import cv2
import numpy
import sys
import pdb
import MotionLib

# working on this 2021-10-16


#-------------------------------------------------------------------------------
def establishBackground(frames):
    medFrame = numpy.median(frames, axis=0);
    backgroundBuffer = numpy.array(medFrame);
    print('Background established')
    return backgroundBuffer

#-------------------------------------------------------------------------------
def showPause(frames):
    num = 1
    for frame in frames:
        #print(frame.shape)
        cv2.imshow(str(num), frame)
        num+=1
    cv2.waitKey(0)
    #cv2.destroyAllWindows()

#-------------------------------------------------------------------------------
def thresholdImage1(background, image):
    mask = numpy.array(MotionLib.findResolvedTarget( image, background, threshold=0.999 ), dtype="uint8")

    metThreshold = False
#    kernel = numpy.ones((2, 2), dtype='uint8')
#    mask = cv2.erode(mask, kernel)
#    mask = cv2.dilate(mask, kernel) > 0

    filteredMask = mask

    #print('Number of thresholded pixels:  ', filteredMask.sum())
    if filteredMask.sum() > 1:
        print("Thresholded", filteredMask.sum())
        metThreshold = True
    return (metThreshold, mask)

#-------------------------------------------------------------------------------
def thresholdImage(background, image):
    
    diff = abs(numpy.array(image, dtype='double') - numpy.array(background, dtype='double'))
    mask = numpy.array((diff * diff).sum(axis=2) > 400, dtype='uint8')

    return mask

#-------------------------------------------------------------------------------
def thresholdSequence(background, frames):
    maskAry = []
    for frame in frames:
        maskAry += [thresholdImage(background, frame)]
    maskAry = numpy.array(maskAry)
    
    medianAry = []
    for i in range(len(maskAry)-3):
        medianAry += [numpy.median(maskAry[i:i+3], axis=0)]
    medianAry += medianAry[-3:]
    medianAry = numpy.array(medianAry)
    print(medianAry.shape)

    sumAry = numpy.sum(medianAry, axis=0)
    notHotPixelMask = sumAry < len(frames)

    finalMask = []
    for mask in maskAry:
        finalMask += [mask * notHotPixelMask]

    return numpy.array(finalMask, dtype='uint8')

#-------------------------------------------------------------------------------
def maskImage(image, mask):
    temp = numpy.array(image)*1.0
    for i in range(3):
        temp[:,:,i] = image[:,:,i] * mask
    return numpy.array(temp, dtype='uint8')

#-------------------------------------------------------------------------------
def processSequence(background, frames):
    kernel5 = numpy.ones((5, 5), dtype='uint8')
    kernel7 = numpy.ones((7, 7), dtype='uint8')
    maskAry = thresholdSequence(background, frames)
    extract = []
    for image,mask in zip(frames,maskAry):

        tempMask = numpy.array(mask, dtype='uint8')
        tempMask = cv2.erode(mask, kernel5)
        tempMask = cv2.dilate(tempMask, kernel7)

        if tempMask.sum() > 2:
            finalImage = maskImage(image, tempMask)
            extract += [finalImage]
    
    return numpy.array(extract, dtype='uint8')

#-------------------------------------------------------------------------------
def processFile(filename):
    inFile = MotionLib.openFile(filename)
    numRead = 1
    frames = MotionLib.readFramesFromFile(inFile, 5)
    background = establishBackground(frames)
    #throwAway = MotionLib.readFramesFromFile(inFile, 42*30) # lady after 42s
    #throwAway = MotionLib.readFramesFromFile(inFile, 91*30) # car after 91s 
    frames = MotionLib.readFramesFromFile(inFile, 30)
    outFile = MotionLib.openOutFile('outfile.avi')
    while len(frames) > 0:
        extract = processSequence(background, frames)
        if len(extract) > 0:
            print('writing frames to file', len(extract))
            MotionLib.writeFramesToOpenFile(outFile, extract)
        frames = MotionLib.readFramesFromFile(inFile, 60)


#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# initialize background frames

print("background read")

processFile('sampleCapture.avi')


print("done")



# vim: ff=unix noswf
