import cv2
import sys
import math
import numpy

import pdb
#
#class Vector(numpy.array):
#    def magnitude():
#        return float(sqrt(self.dot(self)))
#    def norm():
#        return self / self.magnitude()

#-------------------------------------------------------------------------------
def magnitude(a):
    return math.sqrt(a.dot(a))

#-------------------------------------------------------------------------------
def normal(a):
    mag = magnitude(a)
    return (a / mag)

#-------------------------------------------------------------------------------
def normalize(a):
    a = normal(a)
    return a

#-------------------------------------------------------------------------------
def computeProjection( dataV, basisM ):
    # can't I do this with a single matrix op ?
    projection = numpy.zeros(len(dataV))
    #print( 'len bas', len(basis), len(basis[0]), len(dataV))
    for b in basisM.transpose():
        #print('len b', len(b))
        dp = dataV.dot(b)
        projection += b * dp;

    return projection

#-------------------------------------------------------------------------------
def findSpectralMatch( image, backgroundImage ):

    projection = numpy.zeros(len(dataV))
    #print( 'len bas', len(basis), len(basis[0]), len(dataV))
    for b in basisM.transpose():
        #print('len b', len(b))
        dp = dataV.dot(b)
        projection += b * dp;

    return projection

#-------------------------------------------------------------------------------
def readFrames( numFrames ):
    #FRAME_SIZE = 480*640*3  # 480x640 * 3 colors
    vidcap = cv2.VideoCapture(0)  # device 0
    frames = []
    for i in range( numFrames ):
        [success, image] = vidcap.read()
        #print( 'Read a new frame: ', success )
        if len(frames):
            #frames = numpy.append( frames, image )
            frames += [image]
        else:
            frames = [image]
    return numpy.array(frames)

#-------------------------------------------------------------------------------
def openFile(filename):
    inFile = cv2.VideoCapture(filename)
    return inFile

#-------------------------------------------------------------------------------
def readFramesFromFile( inFile, numFrames):
    frames = []
    [success, image] = inFile.read()
    frameCount = 1; 
    while success and frameCount <= numFrames:
        frames += [image]
        [success, image] = inFile.read()
        frameCount += 1

    return numpy.array(frames)

#-------------------------------------------------------------------------------
#def normalizeSpectralFrame( image ):
#    [nrows, ncols, ncolors] = image.shape
#    N = numpy.zeros([nrows, ncols, ncolors])
#    for i in range(nrows):
#        for j in range(ncols):
#            N[i,j,:] = normalize( image[i,j,:] )
#    return N

#-------------------------------------------------------------------------------
def normalize( M ):
    return M / math.sqrt(M.dot(M*1.0))

#-------------------------------------------------------------------------------
def spectralProjection( image, N ):
    [nrows, ncols, ncolors] = image.shape
    
    dotProd = sum(image * N, axis=2)
    for i in range(nrows):
        for j in range(ncols):
            projection[i,j,:] = N[i,j,:] * dotProd[i,j]
    return projection

#-------------------------------------------------------------------------------
def filterFrame( image, background ):
    backNormalized = colorNormalize(background)
    delta = image - spectralProjection(image, backNormalized );
    return delta
    
#-------------------------------------------------------------------------------
#def getDotProduct( image, background ):
#    frameN = colorNormalize(image)
#    dotProd = (frameN * background).sum(axis=2)
#    #threshold = 0.99
#    return dotProd

#-------------------------------------------------------------------------------
def findResolvedTarget( image, background, threshold=0.99 ):
    (backN, magBack) = colorNormalize(background)
    (frameN, magFrame) = colorNormalize(image)
    #temp = image * background
    #dotProd = colorNormalize(temp).sum(axis=2)
    dotProd = (frameN * backN).sum(axis=2)
    #threshold = 0.99
    mask = (dotProd > 0) & (dotProd < threshold) & (magFrame > 50) & (magBack > 50)
    return mask

#-------------------------------------------------------------------------------
def extractResolvedTarget( imageStack, backgroundFrame, threshold=0.99 ):
    #pdb.set_trace()
    backN = colorNormalize(backgroundFrame)
    if imageStack.ndim < 4:
        imageStack = numpy.array([imageStack])
    idx = 0
    kernel = numpy.ones((7, 7), dtype='uint8')
    temp = numpy.array(imageStack * 1.0)
    for image in imageStack:
        mask = numpy.array(findResolvedTarget( 1.0*image, 1.0*backN, threshold=threshold ), dtype='uint8')
        #mask = cv2.dilate(mask, kernel)
        #mask = cv2.erode(mask, kernel) > 0
        for i in range(3):
            temp[idx,:,:,i] = image[:,:,i] * mask
        idx += 1
    return temp

#-------------------------------------------------------------------------------
def extractResolvedTargetSmooth( imageStack, backgroundFrame, threshold=0.99 ):
    #pdb.set_trace()
    backN = colorNormalize(backgroundFrame)
    if imageStack.ndim < 4:
        imageStack = numpy.array([imageStack])
    idx = 0
    temp = numpy.array(imageStack * 1.0)
    for image in imageStack:
        mask = findResolvedTarget( image, backN, threshold=threshold )
        mask = cv2.medianBlur(numpy.array(mask, dtype="uint8"), ksize=5)

        for i in range(3):
            temp[idx,:,:,i] = image[:,:,i] * (mask > 0)
        idx += 1
    return temp

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def openOutFile(filename):
    fps = 30
    framesOut = cv2.VideoWriter( filename, cv2.VideoWriter_fourcc('M','J','P','G'), fps, (640,480) )
    return framesOut

#-------------------------------------------------------------------------------
def writeFramesToOpenFile( outFile, frames ):
    for f in frames:
        outFile.write(numpy.array(f, dtype="uint8"))

#-------------------------------------------------------------------------------
def writeFrames( filename, frames ):
    # filename should end with .avi
    fps = 30
    framesOut = cv2.VideoWriter( filename, cv2.VideoWriter_fourcc('M','J','P','G'), fps, (640,480) )
    #pdb.set_trace()
    for f in frames:
        #foo = numpy.reshape(numpy.uint8(f), (480,640,3))
        #framesOut.write(foo)
        #framesOut.write(f)
        framesOut.write(numpy.array(f, dtype="uint8"))

#-------------------------------------------------------------------------------
#def colorMagnitude( image ):
#    mag = numpy.sqrt( (1.0*image*image).sum(axis=2) )
#    return mag

#-------------------------------------------------------------------------------
def colorNormalize( image ):
    result = numpy.zeros(image.shape)
    [nrows, ncols, ncolors] = image.shape
    image[image<1] = 1
    mag = numpy.sqrt( (1.0*image*image).sum(axis=2) )  # across color axis

    #mag[mag<1] = 1
    for i in range(ncolors):
        result[:,:,i] = 1.0*image[:,:,i] / mag

    return (result, mag)

#def colorNormalize( image ):
#    [nrows, ncols, ncolors] = image.shape
#    result = numpy.zeros(image.shape)
#    for i in range(nrows):
#        for j in range(ncols):
#            result[i,j,:] = (1.0*image[i,j,:]) / math.sqrt( (1.0*image[i,j,:]*image[i,j,:]).sum() )
#    return result

#-------------------------------------------------------------------------------
# can do what I was trying with numpy.median(frames, axis=0)
#def medianPixelsOfFrames( frames ):
#    vecLength = numpy.prod(frames.shape);
#    frameVec = []
#    for f in frames:
#        frameVec += [numpy.reshape(f, vecLength)]
#
#    
#        frameVec = numpy.reshape(f, vecLength)
#    foo = numpy.reshape(numpy.uint8(residualV), (480,640,3))
#    pass
    
#-------------------------------------------------------------------------------
def averageFrames( frames ):
    sumM = numpy.zeros(frames[0].shape)
    maxM = numpy.zeros(frames[0].shape)
    minM = numpy.zeros(frames[0].shape)
    length = len(frames)
    for f in frames:
        maxM = (maxM >= f) * maxM + (f>maxM) * f
        minM = (minM <= f) * minM + (f<minM) * f
        sumM += f
    #avg = (sumM - maxM - minM) / (length -2)
    avg = sumM / length
    return avg

#-------------------------------------------------------------------------------
def avgBackgroundFrame( frames ):
    return averageFrames( frames );
#    sumM = numpy.zeros(frames[0].shape)
#    maxM = numpy.zeros(frames[0].shape)
#    minM = numpy.zeros(frames[0].shape)
#    length = len(frames)
#    for f in frames:
#        maxM = (maxM >= f) * maxM + (f>maxM) * f
#        minM = (minM <= f) * minM + (f<minM) * f
#        sumM += f
#    #avg = (sumM - maxM - minM) / (length -2)
#    avg = sumM / length
#    return avg


#frames = readFrames( 30 );

#-------------------------------------------------------------------------------
# vim: ff=unix noswf
